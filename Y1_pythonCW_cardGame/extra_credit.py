#An own module created that will gerenrate a random deck.
from deck import random_deck

# The random_deck is split into 3 piles.
def split_deck():
    #This makes all of the 3 decks global and used elsehwere in the code.
    global deck_1
    global deck_2
    global deck_3
    #Deals out all of the 21 cards into the 3 decks.
    deck_1=([random_deck[0],random_deck[3],random_deck[6],random_deck[9],random_deck[12],random_deck[15],random_deck[18]])
    deck_2=([random_deck[1],random_deck[4],random_deck[7],random_deck[10],random_deck[13],random_deck[16],random_deck[19]])
    deck_3=([random_deck[2],random_deck[5],random_deck[8],random_deck[11],random_deck[14],random_deck[17],random_deck[20]])
    #Allows the decks to be used.
    return deck_1
    return deck_2
    return deck_3

#called the function to get three decks.
split_deck()

#Asks the user which pile their card is in three times.
for i in range(1,4):
    #shows the decks.
    print("Deck 1: " + str(deck_1))
    print("Deck 2: " + str(deck_2))
    print("Deck 3: " + str(deck_3))
    #Allows the user to input the deck.
    user_input=input("What deck is your chosen card in?\n ")
    #Validates the input so it is either 1,2 or 3.
    correct = False
    while correct == False:
    #used to shuffle deck. The chosen deck will be in the middle.
        if user_input=="1":
            random_deck=deck_2+deck_1+deck_3
            correct = True
        elif user_input=="2":
            random_deck=deck_1+deck_2+deck_3
            correct = True
        elif user_input=="3":
            random_deck=deck_1+deck_3+deck_2
            correct = True
        else:
            #Validates so that either pile 1,2 or 3 is chosen.
            user_input=input("That is not a valid pile.\nPlease pick another pile: ")
            correct = False
        #used to only split the deck 3 times.
        if i < 3:
            split_deck()

#used to determine which card was chosen.
if user_input=="1":
    random_deck=deck_2+deck_1+deck_3
elif user_input=="2":
    random_deck=deck_1+deck_2+deck_3
elif user_input=="3":
    random_deck=deck_1+deck_3+deck_2

#Splits the deck into 7 piles now (With three cards in each).
def split_deck_again():
    global deck_1
    global deck_2
    global deck_3
    global deck_4
    global deck_5
    global deck_6
    global deck_7
    deck_1=(random_deck[0],random_deck[7],random_deck[14])
    deck_2=(random_deck[1],random_deck[8],random_deck[15])
    deck_3=(random_deck[2],random_deck[9],random_deck[16])
    deck_4=(random_deck[3],random_deck[10],random_deck[17])
    deck_5=(random_deck[4],random_deck[11],random_deck[18])
    deck_6=(random_deck[5],random_deck[12],random_deck[19])
    deck_7=(random_deck[6],random_deck[13],random_deck[20])
    return deck_1
    return deck_2
    return deck_3
    return deck_4
    return deck_5
    return deck_6
    return deck_7

split_deck_again()

#Shows all of the seven new decks.
print("There are 7 piles with three cards each")
remaining_piles = [1,2,3,4,5,6,7]
print("These are the piles " + str(remaining_piles))
user_input=input("Pick a pile: ")

#The chosen pile must be 4.
while user_input != "4":
    #Validates so that the user inputs a number from 1 to 7.
    if user_input.isdigit()==False:
        user_input=input("That is not a valid pile.\nPlease pick another pile: ")
    elif int(user_input)<=0:
        user_input=input("That is not a valid pile.\nPlease pick another pile: ")
    elif int(user_input)>=8:
        user_input=input("That is not a valid pile.\nPlease pick another pile: ")
    elif int(user_input) not in remaining_piles:
            print("That pile has been removed")
            user_input=input("Please select one of the remaining piles: ")
    else:
        #Any other valid pile that is not 4 is removed.
        print("I have removed deck " + str(user_input))
        remaining_piles.remove(int(user_input))
        print("The remaining piles are: " + str(remaining_piles))
        user_input=input("Please select one of the remaining piles: ")

#The deck chosen must be deck 4.
random_deck=deck_4

#Once the fourth pile is chosen, the cards will be split again.
remaining_piles=[1,2,3]
user_input=input("There are now only Three cards left.\n Please select a card from" + str(remaining_piles)+":")

#The chosen card must be 2.
while user_input != "2":
    if user_input.isdigit()==False:
        user_input=input("That is not a valid pile.\nPlease pick another pile: ")
    elif int(user_input)<=0:
        user_input=input("That is not a valid pile.\nPlease pick another pile: ")
    elif int(user_input)>=8:
        user_input=input("That is not a valid pile.\nPlease pick another pile: ")
    if user_input == "2":
        print("The card you chose was: " + random_deck[1])
    else:
        remaining_piles.remove(int(user_input))
        print("I have removed card " + str(user_input))
        print("The remaining cards are: " + str(remaining_piles))
        user_input=input("Please select one of the remaining cards: ")
        if int(user_input) not in remaining_piles:
                print("That card has been removed")
                user_input=input("Please select one of the remaining cards: ")

print("The card you chose was: " + random_deck[1])
