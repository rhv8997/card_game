#This module will allow random numbers to be generated.
from random import sample

#REFERECE: http://stackoverflow.com/questions/22842289/generate-n-unique-random-numbers-within-a-range
#This was used to know how to generate unique numbers between 1 and 51 to stop cards being repeatedly added to the randkm deck.
unique_numbers = sample(range(1, 50),21)

#All cards in a deck.
hearts_deck =['ace of hearts','two of hearts','three of hearts','four of hearts','five of hearts','six of hearts','seven of hearts','eight of hearts','nine of hearts','ten of hearts','jack of hearts','queen of hearts','king of hearts']
clubs_deck =['ace of clubs','two of clubs','three of clubs','four of clubs','five of clubs','six of clubs','seven of clubs','eight of clubs','nine of clubs','ten of clubs','jack of clubs','queen of clubs','king of clubs']
diamonds_deck=['ace of diamonds','two of diamonds','three of diamonds','four of diamonds','five of diamonds','six of diamonds','seven of diamonds','eight of diamonds','nine of diamonds','ten of diamonds','jack of diamonds','queen of diamonds','king of diamonds']
spades_deck=['ace of spades','two of spades','three of spades','four of spades','five of spades','six of spades','seven of spades','eight of spades','nine of spades','ten of spades','jack of spades','queen of spades','king of spades']

#all the cards in the deck joined together.
standard_deck=hearts_deck+clubs_deck+diamonds_deck+spades_deck

#21 randomly selected cards from the standard deck split into 3 decks to make is more visually clear.
random_deck_1=[standard_deck[unique_numbers[0]],standard_deck[unique_numbers[3]],standard_deck[unique_numbers[6]],standard_deck[unique_numbers[9]],standard_deck[unique_numbers[12]],standard_deck[unique_numbers[15]],standard_deck[unique_numbers[18]]]
random_deck_2=[standard_deck[unique_numbers[1]],standard_deck[unique_numbers[4]],standard_deck[unique_numbers[7]],standard_deck[unique_numbers[10]],standard_deck[unique_numbers[13]],standard_deck[unique_numbers[16]],standard_deck[unique_numbers[19]]]
random_deck_3=[standard_deck[unique_numbers[2]],standard_deck[unique_numbers[5]],standard_deck[unique_numbers[8]],standard_deck[unique_numbers[11]],standard_deck[unique_numbers[14]],standard_deck[unique_numbers[17]],standard_deck[unique_numbers[20]]]

#all the random cards joined together.
random_deck=(random_deck_1+random_deck_2 + random_deck_3)
