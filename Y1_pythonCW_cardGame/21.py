#An own module created that will gerenrate a random deck.
from deck import random_deck

# The random_deck is split into 3 piles.
def split_deck():
    #This makes all of the 3 decks global and used elsehwere in the code.
    global deck_1
    global deck_2
    global deck_3
    #Deals out all of the 21 cards into the 3 decks.
    deck_1=([random_deck[0],random_deck[3],random_deck[6],random_deck[9],random_deck[12],random_deck[15],random_deck[18]])
    deck_2=([random_deck[1],random_deck[4],random_deck[7],random_deck[10],random_deck[13],random_deck[16],random_deck[19]])
    deck_3=([random_deck[2],random_deck[5],random_deck[8],random_deck[11],random_deck[14],random_deck[17],random_deck[20]])
    #Allows the decks to be used.
    return deck_1
    return deck_2
    return deck_3

#called the function to get three decks.
split_deck()

#Asks the user which pile their card is in three times.
for i in range(1,4):
    #shows the decks.
    print("Deck 1: " + str(deck_1))
    print("Deck 2: " + str(deck_2))
    print("Deck 3: " + str(deck_3))
    #Allows the user to input the deck.
    user_input=input("What deck is your chosen card in?\n ")
    #Validates the input so it is either 1,2 or 3.
    correct = False
    while correct == False:
    #used to shuffle deck. The chosen deck will be in the middle.
        if user_input=="1":
            random_deck=deck_2+deck_1+deck_3
            correct = True
        elif user_input=="2":
            random_deck=deck_1+deck_2+deck_3
            correct = True
        elif user_input=="3":
            random_deck=deck_1+deck_3+deck_2
            correct = True
        else:
            #Validates so that either pile 1,2 or 3 is chosen.
            user_input=input("That is not a valid pile.\nPlease pick another pile: ")
            correct = False
        #used to only split the deck 3 times.
        if i < 3:
            split_deck()

#used to determine which card was chosen.
correct = False

while correct == False:
    if user_input=="1":
        #random_deck put together for the final time and the eleventh card is chosen.
        random_deck=deck_2+deck_1+deck_3
        print("Your card was: " + random_deck[10])
        correct = True
    elif user_input=="2":
        #random_deck put together for the final time and the eleventh card is chosen.
        random_deck=deck_1+deck_2+deck_3
        print("Your card was: " + random_deck[10])
        correct = True
    elif user_input=="3":
        #random_deck put together for the final time and the eleventh card is chosen.
        random_deck=deck_1+deck_3+deck_2
        print("Your card was: " + random_deck[10])
        correct = True
    else:
        #Validates so that either pile 1,2 or 3 is chosen.
        user_input=input("That is not a valid pile.\nPlease pick another pile: ")
        correct = False
